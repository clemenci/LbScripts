#!/bin/sh
# cern-lxdistcc-wrappers.sh: v1 KELEMEN Peter <lxdistcc-admins@cern.ch>
#
# 2012-10-17: Modified by Marco Clemencic
#
_self=${0##*/}

# allow testing without creating symlinks
if [ "$_self" = ".lcg-compiler-wrapper.sh" ] ; then
  _self=${1}
  shift
  set -x
fi

hostos() {
    local arch=$(uname -i)
    local os vers
    case $(lsb_release -si) in
        ScientificCERNSLC)
            os=slc
            vers=$(lsb_release -sr | cut -d. -f1)
            ;;
        *)
            os=$(lsb_release -si | tr '[:upper:]' '[:lower:]')
            vers=$(lsb_release -sr)
    esac
    echo $arch-$os$vers
}

_platform=${LCG_hostos:-$(hostos)}

set_prefix() {
    local _dirname=$1
    local _version=$2

    _prefix=

    local _prefix_tmp
    local search_path=$(echo ${LCG_releases_base} ${LCG_release_area} ${LCG_external_area} | tr : ' ')
    if [ -n "$USE_SFT" ] ; then
      search_path=$(echo $search_path /cvmfs/sft.cern.ch/lcg/{contrib,external,releases})
    fi
    for _prefix_tmp in $search_path ; do
        if [ -d "${_prefix_tmp}/${_dirname}/${_version}/${_platform}" ] ; then
            _prefix="${_prefix_tmp}/${_dirname}/${_version}/${_platform}"
            break
        fi
    done
}

setup_gcc() {
        local _version=$1
        local _no_binutils=$2
        _prefix=""

        # look for a build of the compiler that uses custom binutils
        [ -z "${_no_binutils}" ] && set_prefix gcc ${_version}binutils
        if [ -z "${_prefix}" ] ; then
          # fall back to a compiler with system binutils
          set_prefix gcc ${_version}
        fi

        if [ -e "$_prefix/version.txt" ] ; then
            _binutils_version=$(tr '/' '\n' < "$_prefix/version.txt" | grep binutils | cut -d= -f2 | head -1)
        fi
        # default (if not set)
        : ${_binutils_version:=2.28}
        # FIXME: we have to hardcode the path to the latest binutils, just in case
        LD_LIBRARY_PATH=/cvmfs/lhcb.cern.ch/lib/lcg/releases/binutils/${_binutils_version}/${_platform}/lib${LD_LIBRARY_PATH:+:${LD_LIBRARY_PATH}}
        PATH=/cvmfs/lhcb.cern.ch/lib/lcg/releases/binutils/${_binutils_version}/${_platform}/bin${PATH:+:${PATH}}

        _bin="${_prefix}/bin"
        _lib="${_prefix}/lib64"
        LD_LIBRARY_PATH=$(echo $LD_LIBRARY_PATH | sed 's-[^:]*/gcc/[^:]*:\?--g')
        LD_LIBRARY_PATH="${_lib}${LD_LIBRARY_PATH:+:}${LD_LIBRARY_PATH}"
        PATH="${_bin}${PATH:+:}${PATH}"
        COMPILER_PATH="${_prefix}/lib/gcc/x86_64-unknown-linux-gnu/${_version}"

        GCC_TOOLCHAIN="${_prefix}"
        export LD_LIBRARY_PATH
        export PATH
        export COMPILER_PATH
        export GCC_TOOLCHAIN
}

setup_clang() {
        local _gcc_version _clang_version _base_platform
        _prefix=

        _clang_version=$1
        if [ "${_clang_version}" = "5.0" ] ; then
          _clang_version=5.0.0
        fi

        _base_platform=${_platform}
        for _gcc_version in 7{binutils,} 62{binutils,} 49 48 46 ; do
          _platform=${_base_platform}-gcc${_gcc_version}-opt
          for name in clang llvm ; do
            set_prefix $name ${_clang_version}
            if [ -n "${_prefix}" ] ; then
              break 2
            fi
          done
        done
        _platform=${_base_platform}

        if [ -z "${_prefix}" ] ; then
          set_prefix llvm ${_clang_version}
          case ${_clang_version} in
            3.2) _gcc_version=46 ;;
            3.3|3.4) _gcc_version=48 ;;
            *) _gcc_version=49 ;;
          esac
        fi

        local _llvm_prefix=${_prefix}

        if [ -e "$_prefix/version.txt" ] ; then
            _gcc_full_version=$(tr '/' '\n' < "$_prefix/version.txt" | grep ^gcc | cut -d= -f2 | head -1)
        fi

        if [ -z "${_gcc_full_version}" ] ; then
            local _use_binutils=no-binutils
            if [[ "${_gcc_version}" = *binutils ]] ; then
            _use_binutils=
            fi
            case ${_gcc_version} in
            46) _gcc_full_version=4.6.3 ;;
            48) _gcc_full_version=4.8.1 ;;
            49*) _gcc_full_version=4.9.3 ;;
            62*) _gcc_full_version=6.2.0 ;;
            7* ) _gcc_full_version=7.3.0 ;;
            esac
        fi
        if [ -n "${_gcc_full_version}" ] ; then
            setup_gcc ${_gcc_full_version} ${_use_binutils}
        fi


        _bin="${_llvm_prefix}/bin"
        _lib="${_llvm_prefix}/lib64"
        LD_LIBRARY_PATH=$(echo $LD_LIBRARY_PATH | sed 's-[^:]*/llvm/[^:]*:\?--g')
        LD_LIBRARY_PATH="${_lib}${LD_LIBRARY_PATH:+:}${LD_LIBRARY_PATH}"
        PATH="${_bin}${PATH:+:}${PATH}"

        export LD_LIBRARY_PATH
        export PATH
        export COMPILER_PATH
}

case ${_self} in

        lcg-[cg]++-[0-9].[0-9].[0-9]|lcg-gcc-[0-9].[0-9].[0-9]|lcg-gfortran-[0-9].[0-9].[0-9])

                _version=${_self##*-}
                setup_gcc $_version
                _self=${_self%-*}
                _self=${_self#*-}
                ;;

        lcg-clang-*|lcg-clang++-*)

                _version=${_self##*-}
                setup_clang $_version
                _self=${_self%-*}
                _self=${_self#*-}
                if [ "${_self}" = "clang" -o "${_self}" = "clang++" ] ; then
                  _self="${_self} --gcc-toolchain=${GCC_TOOLCHAIN}"
                fi
                ;;

        *)
                echo "E: Unsupported compiler '${_self}', please contact <marco.clemencic@cern.ch>"
                exit 100
                ;;
esac

# if the lookup failed, we get "/bin" as compiler location, which happens to work on SLC/CentOS
if [ "${_bin}" = "/bin" ] ; then
    echo "error: cannot find ${0##*/}"
    exit 2
fi

exec ${_bin}/${_self} "$@"

# End of file.
